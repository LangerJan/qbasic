CLS
COLOR 14: LOCATE 1, 1: PRINT "(c)1996": LOCATE 1, 30: PRINT "FREEWARE": LOCATE 1, 64: PRINT "Jan Gampe"
VIEW PRINT 2 TO 24
COLOR 15
INPUT "Spieler 1, bitte geben Sie ihren Namen ein!"; NAME1$
CLS
INPUT "Spieler 2, bitte geben Sie ihren Namen ein!"; NAME2$
CLS
PRINT "Wieviele Runden wollen Sie spielen?"
PRINT "1 Runde"
PRINT "3 Runden"
PRINT "6 Runden (Turnier)"
PRINT "12 Runden (gro�es Turnier)"
PRINT "Bitte geben Sie jetzt die Anzahl ein und best�tigen Sie mit -ENTER- !"
PRINT "Falls Sie abbrechen m�chten, geben Sie bitte etwas beliebiges ein,"
PRINT "und best�digen Sie mit -ENTER- !"
INPUT "Anzahl"; ANZAHL
SELECT CASE ANZAHL
       CASE 1: PRINT "OK": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!": SLEEP 2
       CASE 3: PRINT "OK": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!": SLEEP 2
       CASE 6: PRINT "OK": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!": SLEEP 2
       CASE 12: PRINT "OK": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!": SLEEP 2
       CASE ELSE: PRINT "Es wurde etwas falsch eingegeben!": SLEEP 1: SHELL "QBASIC": RUN "ENDE.bas"
END SELECT
LET Z = 0
LET J = 0
LET B = 0
LET N = 0
LET M = 0
LET X = 0
LET c = 0
LET V = 0
LET Q = 0
LET W = 0
FOR P = 1 TO ANZAHL
    CLS
    LET Z = Z + 1
    COLOR 11
    PRINT "Runde "; Z; " l�uft!"
    COLOR 15
    PRINT NAME1$; " bitte -a- f�r Schere,-s- f�r Papier oder -d- f�r Stein"
    PRINT "eingeben und mit -ENTER- best�tigen!"
    COLOR 16
    INPUT eins$
    COLOR 15
    CLS
    PRINT NAME2$; " bitte -1- f�r Schere, -2- f�r Papier oder -3- f�r Stein"
    PRINT "eingeben und mit -ENTER- best�tigen!"
    COLOR 16
    INPUT zwei$
    COLOR 15
    CLS
    LET A$ = "NICHT KOREKT EINGEGEBEN!!!"
    LET Satz1$ = NAME1$ + " hat..."
    SELECT CASE eins$
           CASE "a": PRINT Satz1$
                     COLOR 2
                     PRINT "Schere"
           CASE "s": PRINT Satz1$
                     COLOR 2
                     PRINT "Papier"
           CASE "d": PRINT Satz1$
                     COLOR 2
                     PRINT "Stein"
           CASE ELSE: PRINT Satz1$
                      COLOR 4
                      LET U$ = A$: PRINT U$
    END SELECT
    IF U$ = A$ THEN PRINT "Es wurde etwas falsch eingegeben!": SLEEP 1: SHELL "QBASIC": RUN "ende.bas"
    LET Satz2$ = NAME2$ + " hat..."
    COLOR 15
    SELECT CASE zwei$
           CASE "1": PRINT Satz2$
                     COLOR 2
                     PRINT "Schere"
           CASE "2": PRINT Satz2$
                     COLOR 2
                     PRINT "Papier"
           CASE "3": PRINT Satz2$
                     COLOR 2
                     PRINT "Stein"
           CASE ELSE: PRINT Satz2$
                      COLOR 4
                      LET O$ = A$: PRINT O$
    END SELECT
    IF O$ = A$ THEN PRINT "Es wurde etwas falsch eingegeben!": SLEEP 1: SHELL "QBASIC": RUN "ende.bas"
    IF eins$ = "a" AND zwei$ = "1" THEN COLOR 4: PRINT "Unentschieden! Nochmal!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "a" AND zwei$ = "1" THEN LET Z = Z - 1
    IF eins$ = "a" AND zwei$ = "1" THEN LET P = P - 1
    IF eins$ = "a" AND zwei$ = "1" THEN LET J = J + 1
    IF eins$ = "s" AND zwei$ = "2" THEN COLOR 4: PRINT "Unentschieden! Nochmal!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "s" AND zwei$ = "2" THEN LET Z = Z - 1
    IF eins$ = "s" AND zwei$ = "2" THEN LET P = P - 1
    IF eins$ = "s" AND zwei$ = "2" THEN LET J = J + 1
    IF eins$ = "d" AND zwei$ = "3" THEN COLOR 4: PRINT "Unentschieden! Nochmal!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "d" AND zwei$ = "3" THEN LET Z = Z - 1
    IF eins$ = "d" AND zwei$ = "3" THEN LET P = P - 1
    IF eins$ = "d" AND zwei$ = "3" THEN LET J = J + 1
    IF eins$ = "a" AND zwei$ = "2" THEN LET Q = Q + 1
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "Schere zerschneidet Papier!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "     0     0"
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "      0   0 "
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "       0 0    "
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "        0      "
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "   00  0 0  00    "
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "  0  0     0  0   "
    IF eins$ = "a" AND zwei$ = "2" THEN COLOR 2: PRINT "   00       00    ": COLOR 15
    IF eins$ = "a" AND zwei$ = "3" THEN LET W = W + 1
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "Stein zerschl�gt Schere!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "       000           "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "      000000             "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "    0000000000      "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "   0000000000000               "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "   000000000000  "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "    0000000000             "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "     0000000           "
    IF eins$ = "a" AND zwei$ = "3" THEN COLOR 8: PRINT "       0000          ": COLOR 15
    IF eins$ = "s" AND zwei$ = "1" THEN LET W = W + 1
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "Schere zerschneidet Papier!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "     0     0"
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "      0   0 "
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "       0 0    "
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "        0      "
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "   00  0 0  00    "
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "  0  0     0  0   "
    IF eins$ = "s" AND zwei$ = "1" THEN COLOR 2: PRINT "   00       00    ": COLOR 15
    IF eins$ = "s" AND zwei$ = "3" THEN LET Q = Q + 1
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "Papier umwickelt Stein!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "        000000000000            "
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "       000000000000             "
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "      000000000000            "
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "     000000000000               "
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "    000000000000               "
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "   000000000000                 "
    IF eins$ = "s" AND zwei$ = "3" THEN COLOR 12: PRINT "  000000000000                ": COLOR 15
    IF eins$ = "d" AND zwei$ = "1" THEN LET Q = Q + 1
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "Stein zerschl�gt Schere!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "       000           "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "      000000             "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "    0000000000      "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "   0000000000000               "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "   000000000000  "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "    0000000000    "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "     0000000           "
    IF eins$ = "d" AND zwei$ = "1" THEN COLOR 8: PRINT "       0000          ": COLOR 15
    IF eins$ = "d" AND zwei$ = "2" THEN LET W = W + 1
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "Papier umwickelt Stein!": COLOR 4: PRINT "BITTE KEINE TASTE DR�CKEN!!!"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "        000000000000"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "       000000000000"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "      000000000000"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "     000000000000"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "    000000000000"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "   000000000000"
    IF eins$ = "d" AND zwei$ = "2" THEN COLOR 12: PRINT "  000000000000"
    SELECT CASE eins$
           CASE "a": LET B = B + 1
           CASE "s": LET N = N + 1
           CASE "d": LET M = M + 1
    END SELECT
    COLOR 6
    IF B = 6 THEN PRINT NAME1$; ", warum nimmst du denn so oft Schere?"
    IF N = 6 THEN PRINT NAME1$; ", warum nimmst du denn so oft Papier?"
    IF M = 6 THEN PRINT NAME1$; ", warum nimmst du denn so oft Stein?"
    SELECT CASE zwei$
           CASE "1": LET X = X + 1
           CASE "2": LET c = c + 1
           CASE "3": LET V = V + 1
    END SELECT
    IF X = 6 THEN PRINT NAME2$; ", warum nimmst du denn so oft Schere?"
    IF c = 6 THEN PRINT NAME2$; ", warum nimmst du denn so oft Papier?"
    IF V = 6 THEN PRINT NAME2$; ", warum nimmst du denn so oft Stein?"
    IF Q = 3 THEN PRINT NAME1$; " spielt aber gut!"
    IF Q = 6 THEN PRINT "Puh,"; NAME1$; " hat aber schon oft gewonnen!!"
    IF W = 3 THEN PRINT NAME2$; " spielt aber gut!"
    IF W = 6 THEN PRINT "Puh,"; NAME2$; " hat aber schon oft gewonnen!!"
    IF J = 3 THEN COLOR 9: PRINT "Ihr habt ja immer dasselbe!?!?"
    IF J = 6 THEN COLOR 13: PRINT "Ihr habt aber SEHR oft das gleiche!!!"
    PRINT "Im Moment steht es "; Q; "-"; W; " f�r "; NAME1$; "!"
    SLEEP 10
NEXT P
COLOR 10
IF Q > W THEN PRINT "Sieger ist mit"; Q; " Punkt/Punkten "; NAME1$; "!"
IF Q > W THEN PRINT "Verlierer ist mit"; W; "Punkt/Punkten "; NAME2$; "!"
IF Q < W THEN PRINT "Sieger ist mit"; W; " Punkt/Punkten "; NAME2$; "!"
IF Q < W THEN PRINT "Verlierer ist mit"; Q; "Punkt/Punkten "; NAME1$; "!"
IF Q = W THEN PRINT "Unentschieden mit jeweils"; Q; " Punkt/Punkten!"
SLEEP 3

RUN "ENDE.BAS"
END

